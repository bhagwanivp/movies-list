
export const Loader = () => (
	<div>
        <img src="../images/spinner-loader.svg" alt="loading" />
        <style jsx>{`
        width: 100vw;
        height: 100vh;
        position: fixed;
        left: 0;
        top: 0;
        z-index: 1001;
        background-color: rgba(255, 255, 255, 0.6);
        text-align: center;
    
        img {
            margin: calc(50vh - 64px) auto 0 auto;
            height: 64px;
            background: transparent;
        }
        `}</style>
	</div>
);

export default Loader;
