import Link from 'next/link';

const Header = () => (
  <div>
    <Link href="/">
      <h2>The Open Movie Database</h2>
    </Link>
    <div className='sidenote'>
      {/* <Link href="/">
        <a style={{ marginRight: 15, color: 'white' }}>Home</a>
      </Link>
      <Link href="/details">
        <a style={linkStyle}>Details</a>
      </Link> */}
      by <a href='https://linkedin.com/in/vivekbhagwani' target="_blank">Vivek Bhagwani</a><br />
      <a href='https://gitlab.com/bhagwanivp/movies-list' target="_blank">View Code</a>
    </div>
    
    <style jsx>{`
    h2 {
      margin-right: 10px;
      margin-top: 0;
      cursor: pointer;
    }
    h2:hover {}
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    .sidenote {
      max-width: 130px;
    }
    .sidenote > a {
      color: white;
      padding: 0 0 0 5px;
    }
    `}</style>
  </div>
);

export default Header;
