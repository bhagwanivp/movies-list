import Link from 'next/link'
import { capitalise } from '../utils'
const linkStyle={ margin: '0 5px'}
const showTypes = [
    'movie',
    'series',
    'episode'
]
const showYears = Array.from(Array(133), (n, i) => i+1888)

export const MovieLink = props => (
    <Link href='/details/[id]' as={`/details/${props.id}`}>
        <a style={linkStyle}>{props.id}</a>
    </Link>
)

export const ShowName = props => (
    <label>
        {'Title: '}
        <input type="text" value={props.value} onChange={e => props.setValue(e.target.value)} />
        <style jsx>{`
        input {
            padding: 2px;
            min-width: 200px;
        }
        margin: 3px 3px;
        `}</style>
    </label>
)

export const ShowYearSelect = props => (
    <label>
        {'Release Year: '}
        <select value={props.value} onChange={e => props.setValue(e.target.value)}>
            {showYears.map(t => <option key={t} value={t}>{t}</option>)}
            <option value=''>Select</option>
        </select>
        <style jsx>{`
        margin: 3px 10px;
        `}</style>
    </label>
)

export const ShowTypeSelect = props => (
    <label>
        {'Show Type: '}
        <select value={props.value} onChange={e => props.setValue(e.target.value)}>
            {showTypes.map(t => <option key={t} value={t}>{capitalise(t)}</option>)}
            <option value=''>Select</option>
        </select>
        <style jsx>{`
        margin: 3px 5px;
        `}</style>
    </label>
)

export const PageSelect = props => {
    const optionsList = Array.from(Array(props.totalPages), (_, i) => i+1)
    
    return (<label>
        {'Page: '}
        <button onClick={() => props.setValue(props.value-1)} disabled={props.value === 1}>Previous</button>
        <select value={props.value} onChange={e => props.setValue(e.target.value)}>
            {optionsList.map(t => <option key={t} value={t}>{t}</option>)}
        </select>
        <button onClick={() => props.setValue(props.value+1)} disabled={props.value === props.totalPages}>Next</button>
    </label>)
}

export default {
    MovieLink, ShowName, ShowTypeSelect, ShowYearSelect, capitalise, PageSelect
}