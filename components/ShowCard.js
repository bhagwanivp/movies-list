import Link from 'next/link'

export const ShowCard = ({ Title, Year, Poster, imdbID, Type }) => (
    <Link href='/details/[id]' as={`/details/${imdbID}`}>
        <div className='show-card' >
            <div className='show-info'>
                <h4 className='show-title'>{Title}</h4>
                <h4 className='show-year'>{Year}</h4>
                <p className='show-type'>{Type.toUpperCase()}</p>
            </div>
            <img className='show-poster' src={Poster} alt={Title}/>

            <style jsx>{`
            .show-card {
                cursor: pointer;
                padding: 5px;
                margin: 1px solid black;
                position: relative;
            }
            .show-info {
                width: 100%;
                height: 100%;
                position: absolute;
                top: 0;
                left: 0;
                background-color: rgba(0,0,0,0.8);
                display: none;
                color: white;
                text-align: center;
            }
            
            .show-card:hover {
                transform: scale(1.1);
            }
            .show-card:hover .show-info {
                display: block;
            }
            .show-poster {
                max-height: 220px;
                min-height: 200px;
                width: 100%;
            }
            `}</style>
        </div>
    </Link>
)

export default ShowCard