import Header from './Header';

const Layout = props => (
  <div>
    <Header />
    {props.children}
    <style jsx>{`
    padding: 20px;
    border: 1px solid #DDD;
    color: lightgrey;
    background: #4f4f4f;
    min-height: calc(100vh - 60px);
    `}</style>
  </div>
);

export const withLayout = Page => <Layout><Page /></Layout>

export default Layout;