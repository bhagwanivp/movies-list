import { withLayout } from '../components/Layout'
import { useState, useEffect } from 'react'
import fetch from 'isomorphic-unfetch';

import ShowCard from '../components/ShowCard'
import Loader from '../components/Loader'
import { ShowName, ShowTypeSelect, ShowYearSelect, PageSelect } from '../components'
import { API_BASE_URL, setCookie, getCookie } from '../utils'

export const Index = () => {
    // const cookieData = JSON.parse(getCookie('showSearch')) || {};
    const [showType, setShowType] = useState('')
    const [showTitle, setShowTitle] = useState('batman')
    const [showYear, setShowYear] = useState('')
    const [shows, setShows] = useState([])
    const [loading, setLoading] = useState(false)
    const [totalResults, setTotalResults] = useState()
    const [page, setPage] = useState(1)
    const [error, setError] = useState()

    /* const getLocalSearch = () => {
        const data = JSON.parse(localStorage.getItem('showSearch')) || {}

        setShowTitle(data.showTitle)
        setShowType(data.showType)
        setShowYear(data.showYear)
        setPage(data.page)
    } */

    const getShows = () => {
        let reqUrl = `${API_BASE_URL}`
        if(showTitle !== '') reqUrl += `&s=${showTitle}`
        if(showType !== '') reqUrl += `&t=${showType}`
        if(showYear !== '') reqUrl += `&y=${showYear}`
        if(page > 0) reqUrl += `&page=${page}`

        setCookie('showSearch', JSON.stringify({ showTitle, showType, showYear, page }))

        setLoading(true)
        
        fetch(reqUrl)
        .then(res => res.json())
        .then(response => {
            console.log(reqUrl, response)
            setLoading(false)

            if(response.Response === "True") {
                if(response.hasOwnProperty('Search')) {
                    setShows(response.Search)
                    setTotalResults(Number(response.totalResults))
                } else {
                    setTotalResults(1)
                    setShows([response])
                }
                setError(undefined)
            } else {
                // show error
                setError(response.Error)
                setShows([])
                setTotalResults(0)
            }
        })
        .catch(console.log)
    }

    useEffect(() => {
        getShows()
    }, [showType, showYear, page]);

    return (
        <div>
            <div>
                <ShowName value={showTitle} setValue={setShowTitle} />
                <button onClick={() => {page === 1 ? getShows() : setPage(1);}} >Search</button>
                <ShowYearSelect value={showYear} setValue={setShowYear} />
                <ShowTypeSelect value={showType} setValue={setShowType} />
                <style jsx>{`
                display: flex; 
                flex-wrap: wrap;
                justify-content: flex-start;
                button {
                    margin: 5px 10px;
                    padding: 2px 5px;
                    border-radius: 5px;
                    font-weight: bold;
                }
                `}</style>
            </div>

            <div>
                {loading && <Loader />}
                {error && (
                    <p style={{
                        textAlign: 'center', 
                        fontWeight: 'bold',
                        color: 'red' 
                    }}>ERROR: {error}</p>
                )}
                {shows.length > 0 && !loading && (
                    <div>
                        <div>
                            <p>
                                <span>{totalResults}</span> total, showing <span>{`${page-1}1 - ${shows.length === 10 ? page : page-1}${shows.length % 10}`}</span>
                            </p>
                            {totalResults > 10 && <PageSelect value={page} setValue={setPage} totalPages={totalResults ? Math.ceil(totalResults/10) : 1} />}

                            <style jsx>{`
                            display: flex;
                            justify-content: space-between;
                            align-items: baseline;
                            span {
                                font-size: 1.2rem;
                                font-weight: bold;
                                padding: 0 5px;
                            }
                            `}</style>
                        </div>

                        <div>
                            {shows.map(show => <ShowCard key={show.imdbID} {...show} />)}
                            <style jsx>{`
                            display: grid;
                            grid-gap: 2rem;
                            grid-template-columns: repeat(auto-fit, minmax(150px, 200px));
                            `}</style>
                        </div>
                    </div>
                )}
            </div>
        </div>
    )
}

export default () => withLayout(Index)
