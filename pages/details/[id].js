import { useState, useEffect } from 'react'
import { useRouter } from 'next/router';
import fetch from 'isomorphic-unfetch';
import Loader from '../../components/Loader'

import { API_BASE_URL } from '../../utils'
import { withLayout } from '../../components/Layout'

export const Details = props => {
    const router = useRouter();
    const [id, setId] = useState()
    const [show, setShow] = useState()

    const getShowDetails = () => {
        id && fetch(`${API_BASE_URL}&i=${id}`)
        .then(res => res.json())
        .then(res => {
            res.Response === 'True' && setShow(res)
            console.log(show)
        })
    }

    if(router.query.id && id !== router.query.id) setId(router.query.id)

    useEffect(() => {
        getShowDetails()
    }, [id])

    return (<div className='details-wrapper'>
        {!show && <Loader />}
        {show && <img className='show-poster' src={show.Poster} alt={show.Title}/>}
        {show && <div className='content-wrapper'>
            <span>{show.Rated}</span>
            <h1>{show.Title}, {show.Year}</h1>

            <div className='cards-wrapper'>
                <div className='info-wrapper'>
                    {['Type', 'Released', 'DVD', 'Runtime', 'Genre', 'Language',
                    ].map(sInfo => (
                        <div key={sInfo} className='info'><b>{sInfo}:</b> <p>{show[sInfo]}</p></div>
                    ))}
                </div>

                <div className='ratings-wrapper'>
                    {show.Ratings.map(({ Source, Value }) => (<div key={Source} className='rating'>
                        <b>{Source}</b>
                        <b>{Value}</b>
                    </div>))}
                    <div className='info'><b>IMDB Votes:</b> <p>{show.imdbVotes}</p></div>
                    <br />
                    <div className='info'><b>BoxOffice:</b> <p>{show.BoxOffice}</p></div>
                    <div className='info'><b>Awards:</b> <p>{show.Awards}</p></div>
                </div>

                <div className='metrics-wrapper'></div>
            </div>

            <div className='credits-wrapper'>
                {['Director', 'Actors', 'Writer', 'Production', 'Country', 'Website',
                ].map(sInfo => (
                    <div key={sInfo} className='info'><b>{sInfo}:</b> <p>{show[sInfo]}</p></div>
                ))}
            </div>

            <p className='plot'><b>Plot:</b><br />{show.Plot}</p>
        </div>}
        <style jsx>{`
        position: relative;
        .details-wrapper {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
        }
        .details-wrapper > img {
            margin: 0 auto;
        }
        
        .content-wrapper {
            margin-top: 20px;
        }

        h1 { margin-top: 0; }
        p { margin: 0 }
        b { color: #fda50f; }
        .plot { margin-top: 10px; }
        span {
            position: absolute;
            padding: 10px;
            right: 0;
            top: 0;
            background: rgba(0,0,0,0.5);
        }
        .cards-wrapper {
            display: grid;
            grid-gap: 2rem;
            grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
        }
        .rating, .info {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            margin: 0 0 5px 0;
        }
        @media screen and (min-width: 768px) {
            .content-wrapper {
                width: calc(100vw - 378px);
                margin: 0 0 0 20px;
            }
        }
        `}</style>
    </div>)
}

/* Details.getInitialProps = async function(context) {
    console.log(context)
    const res = await fetch(`${API_BASE_URL}&i=${id}`);
    const data = await res.json();
  
    console.log(`data fetched.`, data);
  
    return {
      show: data
    };
}; */

export default () => withLayout(Details)
