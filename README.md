An app for browsing shows (movies / series / episodes) with the OMDB api.

Setup instructions:
1. clone the repository and cd into it.
2. npm install
3. npm run dev for development server on port 3000.
4. npm start for creating an optimised build and serving it.