const API_KEY = '67bb971d'
export const API_BASE_URL = `https://www.omdbapi.com/?apikey=${API_KEY}`

export const capitalise = str => str.charAt().toUpperCase() + str.slice(1)

export const setCookie = (
    name,
    value,
    days = 7,
    path = window.location.href
  ) => {
    const expires = new Date(Date.now() + days * 864e5).toUTCString();
    document.cookie =
      name +
      "=" +
      encodeURIComponent(value) +
      "; expires=" +
      expires +
      "; path=" +
      path;
  };
  
  export const getCookie = (name) => {
    return document.cookie.split("; ").reduce((r, v) => {
      const parts = v.split("=");
      return parts[0] === name ? decodeURIComponent(parts[1]) : r;
    }, "");
  };
  
  export const deleteCookie = (name, path = window.location.href) => {
    setCookie(name,"", -1, path);
  };

export default {
    API_BASE_URL,
    capitalise,
    setCookie,
    getCookie,
    deleteCookie
}
